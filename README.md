# FT3 Samples
This repository contains several samples which demonstrate basic usage of **FT3** module and client lib.

## Environment setup

In order to run the samples, install Eclipse and PostgreSQL following the [instructions](https://rell.chromia.com/en/master/eclipse/eclipse.html). 

Download [Node.js](https://nodejs.org/en/download/) or install it using [package manager](https://nodejs.org/en/download/package-manager/).

Using Eclipse Marketplace (Help > Eclipse Marketplace) install **nodeclipse** Eclipse plugin.



## Running samples
To import the samples to Eclipse, open Eclipse, select File > Import. In import dialog select 'Git > Projects from Git > Clone URI'. In the dialog enter URI of this repository. 

In the project root directory run:
```
npm install
```
to install JavaScript dependencies (postchain-client, ft3-lib).

Every sample has JavaScript and Rell code. Rell code can be started right clicking on rell/src/main.rell file, and selecting 'Run As > Rell Simple Postchain App'. To start client side code (JavaScript), right click on js file and select Run As > Node Application.

Before starting the rell samples, make sure database is running. 

**Note: Everytime rell sample is started, content of postchain database is deleted.**

## Requirements
* Node.js
* PostgreSQL
* Eclipse 
* Rell Eclipse plugin 
* nodeclipse eclipse plugin