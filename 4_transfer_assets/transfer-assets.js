/**
 * In this example we use operations and methods, provided by ft3 module and ft3 client, to make
 * transfer between two accounts. 
 * 'user.rell' file contains only 'create_user' operation and 'find_by_username' query. Rest of the
 * transfer logic is handled by ft3 module.
 */

const { util } = require('postchain-client');
const {  
  SingleSignatureAuthDescriptor, 
  DirectoryServiceBase, 
  ChainConnectionInfo,
  AssetBalance,
  Blockchain,  
  FlagsType,
  Asset,
  User
} = require('ft3-lib');
 
// define DirectoryService class used by Blockchain class to get postchain node connection info
class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super([
      new ChainConnectionInfo(
        Buffer.from(
          '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',  
          'hex'
        ),
        'http://localhost:7740'
      )
    ]);
  }
}

class UserInfo {
  constructor(displayName, firstName, lastName, age) {
    this.displayName = displayName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  get username() {
    return displayName.toLowerCase();
  }

  serialize() {
    return [this.displayName, this.firstName, this.lastName, this.age];
  }
}

(async () => {
  // Initialize blockchain 
  const blockchain = await Blockchain.initialize(
      Buffer.from('0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 'hex'), 
      new DirectoryService()
  );

  // dapp user 1
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
        keyPair.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // dapp user 2
  const keyPair2 = util.makeKeyPair();
  const user2 = new User(
    keyPair2,
    new SingleSignatureAuthDescriptor(
        keyPair2.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // create blockchain session for each dapp user
  const session = blockchain.newSession(user);
  const session2 = blockchain.newSession(user2);


  // create user accounts
  const userInfo = new UserInfo('john_doe', 'john', 'doe', 30);
  const userInfo2 = new UserInfo('john_doe2', 'John2', 'Doe2', 40);

  try {
    await session.call(
      'create_user',
      userInfo.serialize(),
      user.authDescriptor.toGTV()
    );

    await session2.call(
      'create_user',
      userInfo2.serialize(),
      user2.authDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

	// query blockchain for created users. we need ft3 account ids in order to make a transfer
	let userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

  	let userAccount2 = await blockchain.query('find_by_username', { 
		username: 'john_doe2'
  });
  
  // get ft3 account by account id
  let ftAccount = await session.getAccountById(
    Buffer.from(userAccount.account_id, 'hex')
  );

  // use dev methods to deposit some test tokens to user 1 account
  const asset = await Asset.register('CHROMA', blockchain.id, blockchain);
  await AssetBalance.giveBalance(ftAccount.id_, asset.id, 100, blockchain);

  // transfer 20 CHROMA from user 1 account to user 2 account
  await ftAccount.transfer(
    Buffer.from(userAccount2.account_id, 'hex'),
    asset.id,
    20
  );

  // get user 2 ft3 account and print it to console to verify transfer was successful
  let ftAccount2 = await session2.getAccountById(
    Buffer.from(userAccount2.account_id, 'hex')
  );

  console.log(ftAccount2.assets);
})()
