/**
 * In this example we show how to update data on blockchain. We added 'update_user' operation
 * which updates user attributes. Attributes which should be editable have to be declared 
 * as mutable.
 */

const { util } = require('postchain-client');
const { 
	SingleSignatureAuthDescriptor, 
	DirectoryServiceBase, 
	ChainConnectionInfo,
	Blockchain, 
  FlagsType, 
  User 
} = require('ft3-lib');
 
 
// blockchain connection info
class DirectoryService extends DirectoryServiceBase {
	constructor() {
		super([
			new ChainConnectionInfo(
				Buffer.from(
					'0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',
					'hex'
				),
				'http://localhost:7740'
			),
		]);
	}
}

(async () => {
  // initialize Blockchain object
  const blockchain = await Blockchain.initialize(
      Buffer.from('0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 'hex'), 
      new DirectoryService()
  );

  // create a dapp user
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
        keyPair.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // create user account
  try {
    await blockchain.call(
      user,
      'create_user',
      'john_doe',
      'john',
      'doe',
      30,
      user.authDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

	// get created account and print it to console
	let userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

	console.log(userAccount);

	// update the account
	try {
		await blockchain.call(
			user,
			'update_user',
			'john_doe',
			'John',
			'Doe',
			40,
			user.authDescriptor.hash().toString('hex')
		);
	} catch (error) {
		console.log(`Error creating new account: ${error.message}`);
		process.exit(1);
	}	

	// get updated account details and print them to console
	userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

	console.log(userAccount);
})()
