/**
 * Concept of dapp login is a bit different when compared to traditional centralized apps.
 * Login into a dapp means to provide valid key pair which will be used to sign transactions.
 * If a key pair is not valid the transactions will be rejected by the postchain node.
 * 
 * There are several ways how login could be implemented:
 *  1. automatic login - for dapps which don't require high level of security, during registration
 *     an key pair could be generated, and stored in browser local storage. on each dapp start
 *     key pair would be retrieved from the local storage, and used to sign transactions. 
 *     in case local storage is deleted, key pair is lost and user will not be able to use dapp 
 *     with associate
 *
 *   2. enter private key on login screen - during registration an key pair is generated, which
 *     user has to store on a safe place. On each login, dapp asks user to manually enter the private key.
 *     
 *   3. login using passphrase - in this case, a key pair generated during registration, is encrypted 
 *     and stored in browser local storage. On login, user has to provide passphrase, which is used to 
 *     decrypt private key. from privated key, public key will be derived and then they will be used to 
 *     sign transactions.
 * 
 *   4. single sign-on - on each login, new key pair is generated, and then sso service is asked to
 *     authorize new key pair. when the key pair is authorized, dapp can use it to sign the transactions.
 * 
 * In this example, we'll simulate sso. In Chromia ecosystem, Chromia Vault is used to authorize new key
 * pairs. In user.rell, 'create_user' operation is updated to receive two authentication descriptors.
 * One of them will allow user to use the dapp, and other one will allow user's chromia vault account to
 * add new authenitcation descriptors on the future logins. 
 */

const { util } = require('postchain-client');
const { 
  SingleSignatureAuthDescriptor, 
  DirectoryServiceBase, 
  ChainConnectionInfo,
  Blockchain,
  FlagsType, 
  User
} = require('ft3-lib');
 
class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super([
      new ChainConnectionInfo(
        Buffer.from(
          '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',  
          'hex'
        ),
        'http://localhost:7740'
      )
    ]);
  }
}

class UserInfo {
  constructor(displayName, firstName, lastName, age) {
    this.displayName = displayName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  get username() {
    return displayName.toLowerCase();
  }

  serialize() {
    return [this.displayName, this.firstName, this.lastName, this.age];
  }
}

(async () => {
  // Initialize blockchain object
  const blockchain = await Blockchain.initialize(
      Buffer.from('0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 'hex'), 
      new DirectoryService()
  );

  // when user registers a dapp account, he has to provide his vault account public key
  // in order to allow sso. more info about the flow can be found in FT3 Module doc.
  // in the example we'll generate a new key pair which represents vault account key pair,
  // but in real dapps, a dapp would open the chromia vault to read the public key.

  const vaultKeyPair = util.makeKeyPair();
  const vaultAuthDescriptor = new SingleSignatureAuthDescriptor(
    vaultKeyPair.pubKey,
    [FlagsType.Account, FlagsType.Transfer]
  );


  // another key pair is generated which is used by a user to interact with the blockchain
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
        keyPair.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // create user info for new dapp account
  const userInfo = new UserInfo('john_doe', 'john', 'doe', 30);

  // register account. 
  // note: vault auth descriptor is also added to user account. 
  // with this step registration flow is finished. 
  try {
    await blockchain.call(
      user,
      'create_user',
      userInfo.serialize(),
      user.authDescriptor.toGTV(),
      vaultAuthDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

  // next we show logic which is executed in sso flow. but first we demonstrate
  // how calling 'update_user' operation will fail if a key pair is not authorized 
  // to update an account
  const keyPair2 = util.makeKeyPair();
  const user2 = new User(
    keyPair2,
    new SingleSignatureAuthDescriptor(
        keyPair2.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  userInfo.firstName = 'John';

  // call to 'update_user' operation fails because keyPair2 is not authorized
  try {
    await blockchain.call(
      user2,
      'update_user', 
      userInfo.serialize(),
      user2.authDescriptor.hash()
    );
  } catch {
  }

  userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
  });
  
  // this logic is executed in the Chromia vault when user logs into a dapp using
  // his vault account
  const vaultUser = new User(
    vaultKeyPair,
    vaultAuthDescriptor
  );

  // get ft3 account by id, and using the vault accoun, add authentication descriptor 
  // which corresponds to keyPair2 
  const session = blockchain.newSession(vaultUser);
  const account = await session.getAccountById(
    Buffer.from(userAccount.account_id, 'hex')
  );

  await account.addAuthDescriptor(user2.authDescriptor);

  // now back in a dapp, calling 'update_user' will succeed because keyPair2 is authorized
  try {
    await blockchain.call(
      user2,
      'update_user', 
      userInfo.serialize(),
      user2.authDescriptor.hash()
    );
  } catch {}

  // get account details, print them to console, and verify update was successful
  userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

  console.log(userAccount);
})()
