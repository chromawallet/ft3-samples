/**
 * In real applications where number of user properties can become large, it's not practical
 * to pass every property as an argument to rell operation. In this example it will be
 * shown how to pass user details as single argument to create_user operation. On the 
 * postchain node, user details will be mapped to user_info record during operation invocation. 
 */

const { util } = require('postchain-client');
const {  
  SingleSignatureAuthDescriptor, 
  DirectoryServiceBase, 
  ChainConnectionInfo,
  Blockchain,
  FlagsType, 
  User 
} = require('ft3-lib');
 
// blockchain connection details
class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super([
      new ChainConnectionInfo(
        Buffer.from(
            '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',  
            'hex'
        ),
        'http://localhost:7740'
      )
    ]);
  }
}

// a class used to represent user account details
class UserInfo {
  constructor(displayName, firstName, lastName, age) {
    this.displayName = displayName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  get username() {
    return displayName.toLowerCase();
  }

  // when calling 'create_user' operation, user details will be passed as an array
  // of user properties. order in which properties are passed is important. it has
  // to match the order in which user_info record attributes are declared.
  serialize() {
    return [this.displayName, this.firstName, this.lastName, this.age];
  }
}

(async () => {
  // initialize Blockchain object
  const blockchain = await Blockchain.initialize(
      Buffer.from('0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 'hex'), 
      new DirectoryService()
  );

  // create dapp user
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
        keyPair.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // create a session for the dapp user. BlockchainSession is convenience class added to simplify 
  // interaction with a blockchain. benefit of using session over blockchain object is that 
  // for session object, there is no need to pass User instance, when calling an operation.
  const session = blockchain.newSession(user);

  // user account details
  const userInfo = new UserInfo('john_doe', 'john', 'doe', 30);

  // invoke 'create_user' operation. comparing to 'create_user' calls in previous examples
  // in this case we pass all user details as single argument.
  //
  // as already said, there is no need pass User object as first parameter, because
  // session is initialized with an instance of User object, and all functions which
  // need User object will use that one.
  try {
    await session.call(
      'create_user',
      userInfo.serialize(),
      user.authDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

	// get new user account details and print them to console
	let userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

  console.log(userAccount);
  
  // update user properties
  userInfo.firstName = 'John';
  userInfo.lastName = 'Doe';
  userInfo.age = 40;

  // call update_user to update account details on blockchain
	try {
		await session.call(
			'update_user',
			userInfo.serialize(),
			user.authDescriptor.hash().toString('hex')
		);
	} catch (error) {
		console.log(`Error creating new account: ${error.message}`);
		process.exit(1);
	}	

  // get updated details and print them to console to verify account was successfully updated
	userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

	console.log(userAccount);
})()
