/**
 * This is a simple example which shows basic interaction with a postchain node using 
 * ft3 module and client side lib. user.rell file contains definition for user class
 * which represents dapp account. user class is linked to ft3 account class in order to
 * use account management and payment functionality provided by ft3 module.
 * besides user class definition, the file also contians 'create_user' operation, 
 * which creates a dapp account and 'find_by_username' query which queries user table 
 * by username.
 */

const { util } = require('postchain-client');
const { 
  SingleSignatureAuthDescriptor, 
  DirectoryServiceBase, 
  ChainConnectionInfo,
  Blockchain, 
  FlagsType, 
  User 
} = require('ft3-lib');
 
// blockchain connection info
class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super([
      new ChainConnectionInfo(
        Buffer.from(
          '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',  
          'hex'
        ),
        'http://localhost:7740'
      )
    ]);
  }
}

(async () => {
  // initialize Blockchain object. initialize function receives an id of a blockchain to which 
  // we want to connect and a directory service which holds blockchain connection info.
  // Blockchain class provides functions to call rell operations and queries. 
  // it is used by all ft3 lib entities (Asset, Account, ...) to interact with a blockchain.
  const blockchain = await Blockchain.initialize(
      Buffer.from(
        '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 
        'hex'
      ), 
      new DirectoryService()
  );

  // create a dapp user. an instance of User object holds data needed to sign transaction
  // and to interact with the ft3 lib. it doesn't represent dapp account. with a user object
  // it is possible to access multiple dapp accounts, and a dapp account could have multiple
  // users linked to it (through authentication descriptor).
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
      keyPair.pubKey,
      [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // call 'create_user' operation to create a new dapp account. the first parameter is
  // dapp user. call function uses user keyPair property to sign the transaction, the
  // rest of parameters are passed to blockchain 'create_user' operation.
  try {
    await blockchain.call(
      user,
      'create_user',
      'john_doe',
      'John',
      'Doe',
      30,
      user.authDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

  // get created account details by calling 'find_by_username' query
	const userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

	console.log(userAccount);
})()
