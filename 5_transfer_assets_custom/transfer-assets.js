/**
 * Instead of using ft3 trasnfer operation, in this example we created new transfer operation,
 * to simplify client side logic. Now on the client side we don't access ft3 accounts directly.
 * Added transfer operation handles that part.
 */

const { util } = require('postchain-client');
const { 
  SingleSignatureAuthDescriptor, 
  DirectoryServiceBase, 
  ChainConnectionInfo,
  AssetBalance,
  Blockchain,
  FlagsType,
  Asset, 
  User
} = require('ft3-lib');
 
class DirectoryService extends DirectoryServiceBase {
  constructor() {
    super([
      new ChainConnectionInfo(
        Buffer.from(
          '0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF',  
          'hex'
        ),
        'http://localhost:7740'
      )
    ]);
  }
}

class UserInfo {
  constructor(displayName, firstName, lastName, age) {
    this.displayName = displayName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  get username() {
    return displayName.toLowerCase();
  }

  serialize() {
    return [this.displayName, this.firstName, this.lastName, this.age];
  }
}

(async () => {
  // Initialize blockchain object
  const blockchain = await Blockchain.initialize(
      Buffer.from('0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF', 'hex'), 
      new DirectoryService()
  );

  // Create two dapp users
  const keyPair = util.makeKeyPair();
  const user = new User(
    keyPair,
    new SingleSignatureAuthDescriptor(
        keyPair.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  const keyPair2 = util.makeKeyPair();
  const user2 = new User(
    keyPair2,
    new SingleSignatureAuthDescriptor(
        keyPair2.pubKey,
        [FlagsType.Account, FlagsType.Transfer]
    )
  );

  // create a session for each user
  const session = blockchain.newSession(user);
  const session2 = blockchain.newSession(user2);

  // create user info for new dapp accounts
  const userInfo = new UserInfo('john_doe', 'john', 'doe', 30);
  const userInfo2 = new UserInfo('john_doe2', 'John2', 'Doe2', 40);

  // create accounts
  try {
    await session.call(
      'create_user',
      userInfo.serialize(),
      user.authDescriptor.toGTV()
    );

    await session2.call(
      'create_user',
      userInfo2.serialize(),
      user2.authDescriptor.toGTV()
    );
  } catch (error) {
    console.log(`Error creating new account: ${error.message}`);
    process.exit(1);
  }

  
  // get user 1 details, in order to deposit test tokens
	let userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

  // register asset and deposit test tokens to user 1 account
  const asset = await Asset.register('CHROMA', blockchain.id, blockchain);
  await AssetBalance.giveBalance(
    Buffer.from(userAccount.account_id, 'hex'), 
    asset.id, 
    100, 
    blockchain
  );

  // call new transfer operation, which receives usernames, instead of account ids, as inputs
  await session.call(
    'transfer', 
    'john_doe', 
    'john_doe2', 
    asset.id, 
    20, 
    user.authDescriptor.hash()
  );

  // get user details and print them to verify transfer was successful
  userAccount = await blockchain.query('find_by_username', { 
		username: 'john_doe'
	});

  userAccount2 = await blockchain.query('find_by_username', { 
		username: 'john_doe2'
  });

  console.log(userAccount);
  console.log(userAccount2);
})()
